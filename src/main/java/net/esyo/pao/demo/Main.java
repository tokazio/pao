package net.esyo.pao.demo;

import net.esyo.pao.core.Document;
import net.esyo.pao.core.Fonts;
import static net.esyo.pao.core.HorizontalAlign.CENTER;
import static net.esyo.pao.core.HorizontalAlign.RIGHT;
import net.esyo.pao.core.HzGuide;
import net.esyo.pao.core.Text;
import net.esyo.pao.core.VtGuide;

/**
 *
 * @author romainpetit
 */
public class Main {

    public static void main(String... args) {
        new Main().run();
    }

    private Document buildDoc() {
        final Document doc = new Document();
        doc.add(new Text("Ici le texte (gauche) à déplacer\navec un saut de ligne", 100, 100, 150, -1).font(Fonts.load("/fonts/BerkshireSwash-Regular.ttf")));
        doc.add(new Text("Ici le texte (centré) à déplacer\navec un saut de ligne", 100, 200, 300, -1).align(CENTER));
        doc.add(new Text("Ici le texte (droite) à déplacer\navec un saut de ligne", 100, 300, 300, -1).align(RIGHT));
        doc.add(new VtGuide(40));
        doc.add(new VtGuide(690));
        doc.add(new HzGuide(40));
        doc.add(new HzGuide(200));
        return doc;
    }

    private void run() {
        new MainFormController().useDoc(buildDoc()).show();
    }

}
