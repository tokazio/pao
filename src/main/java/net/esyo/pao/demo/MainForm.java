package net.esyo.pao.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import net.esyo.pao.core.Canvas;

/**
 *
 * @author rpetit
 */
public class MainForm {

    final JFrame frame;
    Canvas pCanvas;

    public MainForm() {
        frame = new JFrame("Pao");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        createLeftMenu(frame);
        createTextMenu(frame);
        createMainMenu(frame);
        createRightPanel(frame);
        frame.setPreferredSize(new Dimension(800, 600));
        frame.pack();
    }


    private void createLeftMenu(JFrame frame) {
        final JPanel pLeftMenu = new JPanel();
        pLeftMenu.setBackground(Color.DARK_GRAY);
        pLeftMenu.setLayout(new BoxLayout(pLeftMenu, BoxLayout.Y_AXIS));
        pLeftMenu.setPreferredSize(new Dimension(50, 600));

        final JToggleButton butElements = new JToggleButton("Elements");
        butElements.setMinimumSize(new Dimension(45, 50));
        pLeftMenu.add(butElements);

        final JToggleButton butTexte = new JToggleButton("Texte");
        butTexte.setMinimumSize(new Dimension(45, 50));
        pLeftMenu.add(butTexte);

        final JToggleButton butImages = new JToggleButton("Images");
        butImages.setMinimumSize(new Dimension(45, 50));
        pLeftMenu.add(butImages);

        frame.add(pLeftMenu, BorderLayout.LINE_START);
    }

    private void createTextMenu(JFrame frame) {
        final JPanel pTextMenu = new JPanel();
        pTextMenu.setBackground(Color.GRAY);
        pTextMenu.setPreferredSize(new Dimension(150, 600));
        frame.add(pTextMenu, BorderLayout.CENTER);
    }

    private void createMainMenu(JFrame frame) {
        final JPanel pMainMenu = new JPanel();
        pMainMenu.setPreferredSize(new Dimension(800, 50));
        frame.add(pMainMenu, BorderLayout.PAGE_START);
    }

    private void createRightPanel(JFrame frame) {
        final JPanel pRight = new JPanel();
        pRight.setLayout(new BorderLayout());
        frame.add(pRight, BorderLayout.LINE_END);
        createToolMenu(pRight);
        createCanvas(pRight);
    }

    private void createToolMenu(JPanel panel) {
        final JPanel pToolMenu = new JPanel();
        pToolMenu.setPreferredSize(new Dimension(800, 50));
        pToolMenu.setBackground(Color.WHITE);
        pToolMenu.setLayout(new BoxLayout(pToolMenu, BoxLayout.X_AXIS));

        final JComboBox cbFont = new JComboBox();
        pToolMenu.add(cbFont);

        final JComboBox cbFontSize = new JComboBox();
        pToolMenu.add(cbFontSize);

        final JButton butFontColor = new JButton();
        pToolMenu.add(butFontColor);

        final JButton butFontBold = new JButton("B");
        pToolMenu.add(butFontBold);

        final JButton butFontItalic = new JButton("I");
        pToolMenu.add(butFontItalic);

        final JButton butFontAlign = new JButton("A");
        pToolMenu.add(butFontAlign);

        final JButton butFontUpper = new JButton();
        pToolMenu.add(butFontUpper);

        final JButton butFontBullet = new JButton("L");
        pToolMenu.add(butFontBullet);

        final JButton butFontSpace = new JButton("Espacement");
        pToolMenu.add(butFontSpace);

        panel.add(pToolMenu, BorderLayout.PAGE_START);
    }

    private void createCanvas(JPanel panel) {
        pCanvas = new Canvas();
        pCanvas.setPreferredSize(new Dimension(150, 600));
        panel.add(pCanvas, BorderLayout.CENTER);
    }
}
