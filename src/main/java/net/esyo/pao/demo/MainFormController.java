package net.esyo.pao.demo;

import net.esyo.pao.core.Document;

/**
 *
 * @author rpetit
 */
public class MainFormController {

    private Document doc = new Document();
    private final MainForm form = new MainForm();

    public MainFormController useDoc(final Document doc) {
        this.doc = doc;
        this.form.pCanvas.useDoc(doc);
        return this;
    }

    public MainFormController show() {
        this.form.frame.setVisible(true);
        return this;
    }
}
