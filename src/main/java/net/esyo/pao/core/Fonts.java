package net.esyo.pao.core;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.mabb.fontverter.FVFont;
import org.mabb.fontverter.FontVerter;

/**
 *
 * @author rpetit
 */
public final class Fonts {

    private static final Logger LOGGER = Logger.getLogger(Fonts.class.getName());
    private static final GraphicsEnvironment GE = GraphicsEnvironment.getLocalGraphicsEnvironment();
    private static final Map<String, Font> FONTS = new TreeMap<>();

    private Fonts() {
        //hide
    }

    public static Font load(final String filename) {
        if (!filename.endsWith(".ttf")) {
            LOGGER.warning("Font file '" + filename + "' must be of .ttf type");
        }
        Font f = FONTS.get(filename);
        if (f == null) {
            try {
                f = Font.createFont(Font.TRUETYPE_FONT, Fonts.class.getResourceAsStream(filename));
            } catch (FontFormatException | IOException ex) {
                LOGGER.log(Level.WARNING, "Can't load font file '" + filename + "'. Will use 'Arial' instead.", ex);
                f = Font.getFont("arial");
            }
            GE.registerFont(f);
            FONTS.put(filename, f);
        }
        return f;
    }

    public static void convert(final String filename) throws IOException {
        FVFont font = FontVerter.convertFont(filename, FontVerter.FontFormat.TTF);
        FileUtils.writeByteArrayToFile(new File(filename + ".ttf"), font.getData());
    }

    public static String[] fontList() {
        return GE.getAvailableFontFamilyNames();
    }
}
