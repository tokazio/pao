package net.esyo.pao.core;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 *
 * @author romainpetit
 */
abstract class AbstractElement implements Element {

    private boolean moving = false;
    protected boolean editing = false;
    private boolean selected = false;
    private boolean hovered = false;

    protected boolean resizingLeft = false;
    protected boolean resizingRight = false;
    protected boolean resizingTop = false;
    protected boolean resizingBottom = false;

    private boolean selectable = true;
    private boolean movable = true;
    private boolean editable = true;

    @Override
    public void select(){
        if(canSelect()){
            this.selected = true;
        }
    }
    
    @Override
    public void deselect(){
        this.selected = false;
    }
    
    protected boolean canSelect() {
        return this.selectable && !this.selected;
    }

    public AbstractElement setSelectable(final boolean value) {
        this.selectable = value;
        return this;
    }

    public AbstractElement setMovable(final boolean value) {
        this.movable = value;
        return this;
    }

    public AbstractElement setEditable(final boolean value) {
        this.editable = value;
        return this;
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }

    @Override
    public Cursor getHoveredCursor() {
        return new Cursor(Cursor.MOVE_CURSOR);
    }

    @Override
    public boolean isMoving() {
        return this.moving;
    }

    @Override
    public boolean isResizing() {
        return this.resizingLeft || this.resizingRight || this.resizingTop || this.resizingBottom;
    }

    @Override
    public boolean isHovered() {
        return this.hovered;
    }

    protected boolean canMove() {
        return this.movable && isSelected() && !isMoving() && !isResizing() && !isEditing();
    }

    @Override
    public boolean isEditing() {
        return this.editing;
    }

    @Override
    public boolean toRemove() {
        return false;
    }

    //Mouse moved, hover me ?
    @Override
    public void onMouseMoved(final Point startClick, final MouseEvent e, final PageInfo info) {
        this.hovered = this.selectable && inRect(e.getPoint());
    }

    @Override
    public void onMousePressed(final MouseEvent e, final PageInfo info) {
        this.selected = canSelect() && inRect(e.getPoint());
    }

    //The mouse is down and moving
    //Then if i can move and the moving start point is on me, i move
    @Override
    public void onMouseDragged(final Point startClick, final MouseEvent e, final PageInfo info) {
        hovered = !isSelected() && this.selectable && overlap(startClick, e.getPoint());
        if (canMove()) {
            this.moving = true;
        }
    }

    //The mouse is now up anywhere
    //deselect me ?
    //if i'm moving -> not deselect
    //if i'm selected -> deselect
    @Override
    public void onMouseReleased(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        /*
        if (isHovered() && canSelect()) {
            this.selected = true;
        }
        */
        if (isSelected() && !isMoving() && !isResizing() && !inRect(e.getPoint())) {
            this.selected = false;
        }
        if (isMoving()) {
            this.moving = false;
        }
    }

}
