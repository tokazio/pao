package net.esyo.pao.core;

/**
 *
 * @author rpetit
 */
public class PageInfo {

    int pageCenter;
    int pageMiddle;

    int canvasWidth;
    int canvasHeight;

    int pageTop;
    int pageBottom;
    int pageLeft;
    int pageRight;
    int pageWidth;
    int pageHeight;

    int mouseX;
    int mouseY;

    boolean mouseDown = false;

    public int getPageCenter() {
        return pageCenter;
    }

    public int getPageMiddle() {
        return pageMiddle;
    }

    public int getCanvasWidth() {
        return canvasWidth;
    }

    public int getCanvasHeight() {
        return canvasHeight;
    }

    public int getPageTop() {
        return pageTop;
    }

    public int getPageBottom() {
        return pageBottom;
    }

    public int getPageLeft() {
        return pageLeft;
    }

    public int getPageRight() {
        return pageRight;
    }

    public int getPageWidth() {
        return pageWidth;
    }

    public int getPageHeight() {
        return pageHeight;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public boolean isMouseDown() {
        return mouseDown;
    }

}
