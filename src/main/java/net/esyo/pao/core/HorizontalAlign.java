package net.esyo.pao.core;

/**
 *
 * @author rpetit
 */
public enum HorizontalAlign {
    LEFT, CENTER, RIGHT
}
