package net.esyo.pao.core;

/**
 *
 * @author rpetit
 */
public enum VerticalAlign {
    TOP, MIDDLE, BOTTOM
}
