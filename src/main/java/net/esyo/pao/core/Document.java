package net.esyo.pao.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author romainpetit
 */
public class Document {

    private final List<Canvas> drawListeners = new ArrayList<>();
    private final List<Page> pages = new ArrayList<>();
    private Page currentPage = new Page();

    public Document() {
        pages.add(currentPage);
    }

    public Document addPage(Page page){
        this.pages.add(page);
        return this;
    }

    public void add(Element el) {
        currentPage.add(el);
    }

    void addDrawListener(Canvas canvas) {
        drawListeners.add(canvas);
    }

    public boolean noneSelected() {
        return getElements().stream().noneMatch(el -> el.isSelected());
    }

    public List<Element> getElements() {
        return currentPage.getElements();
    }

    public List<Guide> getGuides() {
        final List<Guide> guides = new ArrayList<>();
        currentPage.getElements().stream().filter(el -> el instanceof Guide).forEach(el -> guides.add((Guide) el));
        return guides;
    }

    void remove(Element el) {
        currentPage.remove(el);
    }

    void removeAll(Collection<Element> list) {
        currentPage.removeAll(list);
    }

}
