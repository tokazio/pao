package net.esyo.pao.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author romainpetit
 */
class Page {

    private final List<Group> groups = new ArrayList<>();
    private final List<Element> elements = new ArrayList<>();

    public void addGroup(Group group) {
        this.groups.add(group);
    }

    public void add(Element el) {
        elements.add(el);
    }

    public List<Element> getElements() {
        return Collections.unmodifiableList(elements);
    }

    public void remove(Element el) {
        elements.remove(el);
    }

    public void removeAll(Collection<Element> list) {
        elements.removeAll(list);
    }
}
