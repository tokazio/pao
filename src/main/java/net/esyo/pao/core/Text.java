package net.esyo.pao.core;

import java.awt.Color;
import java.awt.Font;
import static java.awt.Font.PLAIN;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.text.AttributedString;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTextArea;
import static net.esyo.pao.core.HorizontalAlign.CENTER;
import static net.esyo.pao.core.HorizontalAlign.LEFT;
import static net.esyo.pao.core.HorizontalAlign.RIGHT;
import static net.esyo.pao.core.VerticalAlign.BOTTOM;
import static net.esyo.pao.core.VerticalAlign.MIDDLE;
import static net.esyo.pao.core.VerticalAlign.TOP;

/**
 *
 * @author romainpetit
 */
public class Text extends AbstractRectangularElement {

    private List<String> text;
    private final Color color = Color.BLACK;
    private Font font = new Font("arial", PLAIN, 12);
    private HorizontalAlign hzAlign = LEFT;

    private final JTextArea area = new JTextArea();

    public Text(final String text, final int x, final int y, final int width, final int height) {
        super(x, y, width, height);
        this.text = Arrays.asList(text.split("\\n"));
    }

    @Override
    public void draw(final Graphics2D g, final PageInfo info) {
        getSize(g);
        if (isMoving() || isResizing()) {
            g.setColor(Color.GRAY);
            g.setFont(new Font("arial", PLAIN, 8));
            g.setStroke(Utils.dashed(3f, 3f));
            drawPosition(g, info);
            g.setStroke(Utils.normal());
        }
        if (isSelected() || isHovered()) {
            g.setStroke(Utils.dashed(3f, 3f));
            if (isSelected()) {
                g.setColor(Color.DARK_GRAY);
            } else if (isHovered()) {
                g.setColor(Color.LIGHT_GRAY);
            }
            g.drawRect(left, top, width, height);
            g.setStroke(Utils.normal());
            if (isSelected()) {
                if (this.resizingRight) {
                    g.setColor(Color.ORANGE);
                } else {
                    g.setColor(Color.BLACK);
                }
                g.fillArc(left + width - HANDLE, top + height / 2 - HANDLE, 2 * HANDLE, 2 * HANDLE, 180 + 90, 180);
                if (this.resizingLeft) {
                    g.setColor(Color.ORANGE);
                } else {
                    g.setColor(Color.BLACK);
                }
                g.fillArc(left - HANDLE, top + height / 2 - HANDLE, 2 * HANDLE, 2 * HANDLE, 90, 180);
            }
        }
        drawParagraph(g, true);
        if (isMoving() && vtCenterOn(info.pageCenter)) {
            g.setColor(Color.MAGENTA);
            g.setStroke(Utils.dashed(3f, 3f));
            g.drawLine(info.getPageCenter(), 0, info.getPageCenter(), info.getCanvasHeight());
            g.setStroke(Utils.normal());
        }
    }

    private void drawParagraph(final Graphics2D g, final boolean draw) {
        int decx = 0;
        int y = getTop();
        for (String str : text) {
            final AttributedString att = new AttributedString(str);
            att.addAttribute(TextAttribute.FONT, this.font);
            att.addAttribute(TextAttribute.FOREGROUND, this.color);
            final LineBreakMeasurer linebreaker = new LineBreakMeasurer(att.getIterator(), g.getFontRenderContext());
            while (linebreaker.getPosition() < str.length()) {
                final TextLayout textLayout = linebreaker.nextLayout(this.width);
                y += textLayout.getAscent();
                if (draw) {
                    if (this.hzAlign == CENTER) {
                        decx = (int) ((this.width - textLayout.getAdvance()) / 2d);
                    }
                    if (this.hzAlign == RIGHT) {
                        decx = (int) (this.width - textLayout.getAdvance());
                    }
                    textLayout.draw(g, getLeft() + decx, y);
                }
                y += textLayout.getDescent() + textLayout.getLeading();
            }
        }
        this.height = y - getTop();
    }

    @Override
    public void drawPosition(final Graphics2D g, final PageInfo info) {
        if (isResizing()) {
            int y;
            VerticalAlign v;
            if (getMiddle() >= info.getPageMiddle()) {
                y = getTop() - HANDLE;
                v = BOTTOM;
            } else {
                y = getBottom() + HANDLE;
                v = TOP;
            }
            Utils.drawText(g, getCenter(), y, Integer.toString(getWidth()) + "," + Integer.toString(getHeight()), CENTER, v);
            Utils.drawText(g, getLeft() - HANDLE * 2, getMiddle(), Integer.toString(getLeft() - info.pageLeft), RIGHT, MIDDLE);
            Utils.drawText(g, getRight() + HANDLE * 2, getMiddle(), Integer.toString(info.pageRight - getRight()), LEFT, MIDDLE);
        } else {
            Utils.drawText(g, getLeft() - HANDLE, getTop(), Integer.toString(getLeft() - info.pageLeft) + "," + Integer.toString(getTop() - info.pageTop), RIGHT, BOTTOM);
            Utils.drawText(g, getRight() + HANDLE, getBottom(), Integer.toString(info.pageRight - getRight()) + "," + Integer.toString(info.pageBottom - getBottom()), LEFT, TOP);
        }
    }

    public Text align(final HorizontalAlign align) {
        this.hzAlign = align;
        return this;
    }

    public Text font(final Font font) {
        this.font = font.deriveFont(this.font.getStyle(), this.font.getSize());
        return this;
    }

    @Override
    public void onDoubleClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        /*
        if (selected) {
            area.setFont(font);
            area.setBounds(left, top, width, height);
            area.setText(text);
            canvas.add(area);
            editing = true;
            area.requestFocus();
            System.out.println("start editing " + this);
        }
         */
    }

    private void getSize(final Graphics g) {
        if (this.height == -1) {
            drawParagraph((Graphics2D) g, false);
        }
    }

    @Override
    public void onClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        //do nothing
    }

    @Override
    public void onMouseReleased(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        super.onMouseReleased(canvas, e, info);
        if (isEditing()) {
            canvas.remove(area);
            this.text = Arrays.asList(area.getText().split("\\n"));
            this.editing = false;
        }
    }

}
