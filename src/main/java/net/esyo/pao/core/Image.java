package net.esyo.pao.core;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

/**
 *
 * @author romainpetit
 */
class Image extends AbstractRectangularElement {

    public Image(final int x, final int y, final int width, final int height) {
        super(x, y, width, height);
    }

    @Override
    public void draw(final Graphics2D g, final PageInfo info) {

    }

    @Override
    public void drawPosition(final Graphics2D g, final PageInfo info) {

    }

    @Override
    public void onDoubleClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {

    }

    @Override
    public void onClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {

    }

}
