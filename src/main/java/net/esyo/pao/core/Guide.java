package net.esyo.pao.core;

/**
 *
 * @author rpetit
 */
public interface Guide extends Element {

    boolean overlap(final Guide g);

}
