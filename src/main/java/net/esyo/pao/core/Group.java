package net.esyo.pao.core;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author romainpetit
 */
class Group {

    private final List<Element> elements = new ArrayList<>();

    public void addGroup(final Element element) {
        this.elements.add(element);
    }

}
