package net.esyo.pao.core;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import static java.awt.Font.PLAIN;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 *
 * @author romainpetit
 */
abstract class AbstractGuide implements Guide {

    protected int y;
    protected int x;
    protected int h;
    protected int w;
    protected boolean selected = false;
    protected boolean hovered = false;
    protected boolean moving = false;
    protected boolean removeMe = false;
    protected int startX;
    protected int startY;

    abstract boolean isRemovable(final PageInfo info);

    protected AbstractGuide(final int x, final int y, final int w, final int h) {
        this.y = y;
        this.x = x;
        this.h = h;
        this.w = w;
    }
    
    @Override
    public void select(){
        this.selected = true;
    }
    
    @Override
    public void deselect(){
        this.selected = false;
    }

    protected Guide created() {
        this.selected = true;
        this.hovered = true;
        this.moving = true;
        this.startX = x;
        this.startY = y;
        return this;
    }

    protected abstract boolean inPage(final PageInfo info);

    @Override
    public void draw(final Graphics2D g, final PageInfo info) {
        if (isHovered() || (isMoving() && inPage(info))) {
            g.setColor(Color.GRAY);
            g.setFont(new Font("arial", PLAIN, 8));
            drawPosition(g, info);
        }
        g.setColor(Color.CYAN);
        if (isHovered()) {
            g.setColor(Color.ORANGE);
        }
    }

    @Override
    public boolean toRemove() {
        return this.removeMe;
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }

    @Override
    public void snapCenterTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public void snapMiddleTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public void snapLeftTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public void snapRightTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public void snapTopTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public void snapBottomTo(final Point start, final int pos) {
        throw new UnsupportedOperationException("Can't snap a guide.");
    }

    @Override
    public boolean isMoving() {
        return this.moving;
    }

    @Override
    public boolean isResizing() {
        return false;
    }

    @Override
    public boolean isHovered() {
        return this.hovered;
    }

    @Override
    public Cursor getHzResizeCursor() {
        return new Cursor(Cursor.S_RESIZE_CURSOR);
    }

    @Override
    public Cursor getVtResizeCursor() {
        return new Cursor(Cursor.E_RESIZE_CURSOR);
    }

    @Override
    public void onDoubleClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        //doNothing
    }

    @Override
    public boolean isEditing() {
        return false;
    }

    @Override
    public boolean overlap(final Point start, final Point end) {
        return false;
    }

    @Override
    public int getMiddle() {
        throw new UnsupportedOperationException("No middle for a guide.");
    }

    @Override
    public int getCenter() {
        throw new UnsupportedOperationException("No center for a guide.");
    }

    @Override
    public int getHeight() {
        throw new UnsupportedOperationException("No width for a guide.");
    }

    @Override
    public int getWidth() {
        throw new UnsupportedOperationException("No width for a guide."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onClick(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        //do nothing
    }

    @Override
    public void onMouseDragged(final Point startClick, final MouseEvent e, final PageInfo info) {
        if (isSelected() && !isMoving() && inRect(startClick)) {
            this.moving = true;
        }
    }

    @Override
    public void onMouseMoved(final Point startClick, final MouseEvent e, final PageInfo info) {
        this.hovered = inRect(e.getPoint());
    }

    @Override
    public void onMousePressed(final MouseEvent e, final PageInfo info) {
        if (!isSelected() && inRect(e.getPoint())) {
            this.selected = true;
        }
        this.startX = this.x;
        this.startY = this.y;
    }

    @Override
    public void onMouseReleased(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        this.removeMe = isSelected() && isRemovable(info);
        if (!isMoving() && !inRect(e.getPoint())) {
            this.selected = false;
        }
        this.moving = false;
    }

}
