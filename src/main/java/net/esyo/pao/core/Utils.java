package net.esyo.pao.core;

import java.awt.BasicStroke;
import java.awt.Graphics2D;

/**
 *
 * @author rpetit
 */
public final class Utils {

    private Utils() {
        //hide
    }

    public static BasicStroke dashed(final float lineSize, final float spaceSize) {
        final float dash1[] = {lineSize, spaceSize};
        return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
    }

    public static BasicStroke axe() {
        final float dash1[] = {3, 3, 9, 3};
        return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
    }

    public static BasicStroke normal() {
        return new BasicStroke(1);
    }

    public static void drawText(final Graphics2D g, final int x, final int y, final String text, final HorizontalAlign hAlign, final VerticalAlign vAlign) {
        int dx = 0;
        int dy = 0;
        switch (hAlign) {
            case CENTER:
                dx = -(int) (g.getFontMetrics().stringWidth(text) / 2);
                break;
            case RIGHT:
                dx = -g.getFontMetrics().stringWidth(text);
                break;
        }
        switch (vAlign) {
            case BOTTOM:
                dy = -g.getFontMetrics().getHeight();
                break;
            case MIDDLE:
                dy = -(int) (g.getFontMetrics().getHeight() / 2);
                break;
        }
        g.drawString(text, x + dx, y + dy + g.getFontMetrics().getAscent());
    }
}
