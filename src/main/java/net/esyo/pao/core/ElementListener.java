package net.esyo.pao.core;

/**
 *
 * @author rpetit
 */
public interface ElementListener {

    void onMoved();

    void onCreated();

    void onDeleted();

    void onResized();

    void onSnapped();

    void onEdited();
}
