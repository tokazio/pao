package net.esyo.pao.core;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import static net.esyo.pao.core.HorizontalAlign.LEFT;
import static net.esyo.pao.core.HorizontalAlign.RIGHT;
import static net.esyo.pao.core.VerticalAlign.MIDDLE;

/**
 *
 * @author romainpetit
 */
public class VtGuide extends AbstractGuide {

    private static final int HANDLE = 2;

    public VtGuide(final int x) {
        super(x, -1, -1, -1);
    }

    @Override
    public void draw(final Graphics2D g, final PageInfo info) {
        super.draw(g, info);
        g.drawLine(getX(), 0, getX(), info.getCanvasHeight());
    }

    @Override
    public void drawPosition(final Graphics2D g, final PageInfo info) {
        Utils.drawText(g, getX() - 20, info.getMouseY(), Integer.toString(getX() - info.getPageLeft()), RIGHT, MIDDLE);
        Utils.drawText(g, getX() + 20, info.getMouseY(), Integer.toString(info.getPageRight() - getX()), LEFT, MIDDLE);
    }

    @Override
    protected boolean inPage(final PageInfo info) {
        return getX() > info.getPageLeft() && getX() < info.getPageRight();
    }

    @Override
    boolean isRemovable(final PageInfo info) {
        return getX() < info.getPageLeft() || getX() > info.getPageRight();
    }

    @Override
    public boolean inRect(final Point point) {
        return point.x > getX() - HANDLE && point.x < getX() + HANDLE;
    }

    @Override
    public boolean overlap(final Guide guide) {
        if (guide != this && guide instanceof VtGuide) {
            return guide.getLeft() >= getX() - HANDLE && guide.getRight() <= getX() + HANDLE;
        }
        return false;
    }

    public int getX() {
        return this.x;
    }

    @Override
    public int getLeft() {
        return this.x;
    }

    @Override
    public int getRight() {
        return this.x;
    }

    @Override
    public int getTop() {
        throw new UnsupportedOperationException("Vertical guide has no top side.");
    }

    @Override
    public int getBottom() {
        throw new UnsupportedOperationException("Vertical guide has no bottom side.");
    }

    @Override
    public Cursor getHoveredCursor() {
        return new Cursor(Cursor.E_RESIZE_CURSOR);
    }

    @Override
    public void onMouseDragged(final Point start, final MouseEvent e, final PageInfo info) {
        super.onMouseDragged(start, e, info);
        if (isMoving()) {
            this.x = this.startX + e.getPoint().x - start.x;
        }
    }

}
