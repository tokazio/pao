package net.esyo.pao.core;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 *
 * @author romainpetit
 */
public interface Element {

    void draw(final Graphics2D g, final PageInfo info);

    void drawPosition(final Graphics2D g, final PageInfo info);

    void onMouseDragged(final Point startClick, final MouseEvent e, final PageInfo info);

    void onMouseMoved(final Point startClick, final MouseEvent e, final PageInfo info);

    void onMousePressed(final MouseEvent e, final PageInfo info);

    void onDoubleClick(final Canvas canvas, final MouseEvent e, final PageInfo info);

    void onClick(final Canvas canvas, final MouseEvent e, final PageInfo info);

    void onMouseReleased(final Canvas canvas, final MouseEvent e, final PageInfo info);

    boolean inRect(final Point p);

    boolean overlap(final Point selectionStart, final Point selectionEnd);

    boolean isSelected();

    int getLeft();

    int getRight();

    int getTop();

    int getBottom();

    int getMiddle();

    int getCenter();

    int getHeight();

    int getWidth();

    void snapCenterTo(final Point start, final int pos);

    void snapMiddleTo(final Point start, final int pos);

    void snapLeftTo(final Point start, final int pos);

    void snapRightTo(final Point start, final int pos);

    void snapTopTo(final Point start, final int pos);

    void snapBottomTo(final Point start, final int pos);

    boolean isMoving();

    boolean isResizing();

    boolean isEditing();

    boolean isHovered();

    Cursor getHoveredCursor();

    Cursor getHzResizeCursor();

    Cursor getVtResizeCursor();

    boolean toRemove();

    void select();

    void deselect();
}
