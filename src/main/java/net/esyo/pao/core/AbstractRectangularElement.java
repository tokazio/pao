package net.esyo.pao.core;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 *
 * @author romainpetit
 */
public abstract class AbstractRectangularElement extends AbstractElement {

    protected static final int HANDLE = 5;

    protected int top;
    protected int left;
    protected int width;
    protected int height;

    protected int startTop;
    protected int startLeft;
    protected int startWidth;
    protected int startHeight;

    public AbstractRectangularElement(final int x, final int y, final int width, final int height) {
        this.top = y;
        this.left = x;
        this.width = width;
        this.height = height;
    }

    @Override
    public final int getMiddle() {
        return (int) (getTop() + (getHeight() / 2));
    }

    @Override
    public final int getCenter() {
        return (int) (getLeft() + (getWidth() / 2));
    }

    @Override
    public final int getWidth() {
        return this.width;
    }

    @Override
    public final int getHeight() {
        return this.height;
    }

    @Override
    public final int getLeft() {
        return this.left;
    }

    //todo test
    @Override
    public final int getRight() {
        return getLeft() + getWidth();
    }

    @Override
    public final int getTop() {
        return this.top;
    }

    //todo test
    @Override
    public final int getBottom() {
        return getTop() + getHeight();
    }

    @Override
    public void snapCenterTo(final Point start, final int pos) {
        this.left = (int) (pos - getWidth() / 2);
    }

    @Override
    public void snapMiddleTo(final Point start, final int pos) {
        this.top = (int) (pos - getHeight() / 2);
    }

    @Override
    public void snapLeftTo(final Point start, final int pos) {
        this.left = pos;
    }

    @Override
    public void snapRightTo(final Point start, final int pos) {
        this.width = pos - getLeft();
    }

    @Override
    public void snapTopTo(final Point start, final int pos) {
        this.top = pos;
    }

    @Override
    public void snapBottomTo(final Point start, final int pos) {
        this.height = pos - getTop();
    }

    @Override
    public Cursor getHzResizeCursor() {
        return new Cursor(Cursor.S_RESIZE_CURSOR);
    }

    @Override
    public Cursor getVtResizeCursor() {
        return new Cursor(Cursor.E_RESIZE_CURSOR);
    }

    public boolean vtCenterOn(final int x) {
        return getCenter() > x - HANDLE && getCenter() < x + HANDLE;
    }

    public boolean hzCenterOn(final int y) {
        return y == getMiddle();
    }

    //todo test U
    @Override
    public boolean inRect(final Point point) {
        return point.x > getLeft() && point.x < getRight() && point.y > getTop() && point.y < getBottom();
    }

    //todo test U
    @Override
    public boolean overlap(final Point selectionStart, final Point selectionEnd) {
        final Point l = new Point(selectionStart.x > selectionEnd.x ? selectionEnd.x : selectionStart.x, selectionStart.y > selectionEnd.y ? selectionEnd.y : selectionStart.y);
        final Point r = new Point(selectionStart.x > selectionEnd.x ? selectionStart.x : selectionEnd.x, selectionStart.y > selectionEnd.y ? selectionStart.y : selectionEnd.y);
        return !(r.y < getTop() || l.y > getBottom() || r.x < getLeft() || l.x > getRight());
    }

    @Override
    public void onMousePressed(final MouseEvent e, final PageInfo info) {
        super.onMousePressed(e, info);
        if (isSelected() && !isResizing() && e.getPoint().x < getLeft() && e.getPoint().x > getLeft() - HANDLE && e.getPoint().y > getTop() && e.getPoint().y < getBottom()) {
            this.resizingLeft = true;
        }
        if (isSelected() && !isResizing() && e.getPoint().x > getRight() && e.getPoint().x < getRight() + HANDLE && e.getPoint().y > getTop() && e.getPoint().y < getBottom()) {
            this.resizingRight = true;
        }
        this.startTop = getTop();
        this.startHeight = getHeight();
        this.startLeft = getLeft();
        this.startWidth = getWidth();
    }

    @Override
    public void onMouseDragged(final Point start, final MouseEvent e, final PageInfo info) {
        super.onMouseDragged(start, e, info);
        if (isSelected() && !isEditing()) {

            //todo
            /*
            if (!resizing() && p.x > left + width && p.x < left + width + 5 && p.y > top && p.y < top + height) {
                resizingRight = true;
            }
            if (!resizing() && p.x > left + width && p.x < left + width + 5 && p.y > top && p.y < top + height) {
                resizingRight = true;
            }
             */
            if (isMoving()) {
                top = startTop + e.getPoint().y - start.y;
                left = startLeft + e.getPoint().x - start.x;
            } else if (resizingLeft) {
                left = startLeft + e.getPoint().x - start.x;
                width = startWidth - (e.getPoint().x - start.x);
                if (width <= 1) {
                    left = startLeft + startWidth - 1;
                    width = 1;
                }
            } else if (resizingRight) {
                width = startWidth + e.getPoint().x - start.x;
                if (width <= 1) {
                    width = 1;
                }
            }
        }
    }

    @Override
    public void onMouseReleased(final Canvas canvas, final MouseEvent e, final PageInfo info) {
        super.onMouseReleased(canvas, e, info);
        this.resizingLeft = false;
        this.resizingRight = false;
        this.resizingTop = false;
        this.resizingBottom = false;
    }

}
