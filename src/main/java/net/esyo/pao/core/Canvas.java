package net.esyo.pao.core;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JPanel;

/**
 *
 * @author rpetit
 */
public class Canvas extends JPanel implements MouseListener, MouseMotionListener {

    private static final int HANDLE = 5;

    private Point startClick = new Point(0, 0);
    private Point endClick = new Point(0, 0);

    private final int marginTop = 80;
    private final int marginLeft = 80;
    private final int marginRight = 80;
    private final int marginBottom = 80;
    private final PageInfo info = new PageInfo();
    private Document doc;

    public Canvas() {
        super.setLayout(null);
        init();
    }

    private void init() {
        super.addMouseListener(this);
        super.addMouseMotionListener(this);
    }

    public Canvas useDoc(final Document doc) {
        this.doc = doc;
        doc.addDrawListener(this);
        return this;
    }

    //https://docs.oracle.com/javase/tutorial/2d/advanced/quality.html
    private static final RenderingHints ANTI_ALIAS = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    private static final RenderingHints ANTI_ALIAS_TEXT = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    private static final RenderingHints SPEED = new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);

    private void initGraphics(final Graphics graphics) {
        //((Graphics2D) graphics).addRenderingHints(ANTI_ALIAS);
        ((Graphics2D) graphics).addRenderingHints(ANTI_ALIAS_TEXT);
        ((Graphics2D) graphics).addRenderingHints(SPEED);
    }
    
    private void selectAll(){
        for(Element el : doc.getElements()){
            el.select();
        }
    }
    
    private void deselectAll(){
        for(Element el : doc.getElements()){
            el.deselect();
        }
    }

    @Override
    public void paintComponent(final Graphics g) {
        initGraphics(g);
        info.canvasWidth = getWidth();
        info.canvasHeight = getHeight();
        info.pageWidth = getWidth() - marginLeft - marginRight;
        info.pageHeight = getHeight() - marginTop - marginBottom;
        info.pageBottom = marginTop + info.pageHeight;
        info.pageTop = marginTop;
        info.pageLeft = marginLeft;
        info.pageRight = marginLeft + info.pageWidth;
        info.pageCenter = (int) (info.pageWidth / 2d + marginLeft);
        info.pageMiddle = (int) (info.pageHeight / 2d + marginTop);

        //background
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, getWidth(), getHeight());

        //page
        g.setColor(Color.WHITE);
        g.fillRect(info.pageTop, info.pageLeft, info.pageWidth, info.pageHeight);

        //elements
        doc.getElements().forEach(e -> e.draw((Graphics2D) g, info));

        //selection
        if (info.mouseDown && doc.noneSelected()) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(startClick.x, startClick.y, startClick.x, info.mouseY);
            g.drawLine(startClick.x, info.mouseY, info.mouseX, info.mouseY);
            g.drawLine(info.mouseX, info.mouseY, info.mouseX, startClick.y);
            g.drawLine(info.mouseX, startClick.y, startClick.x, startClick.y);
        }
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        if (e.getClickCount() >= 2) {
            for (Element el : doc.getElements()) {
                el.onDoubleClick(this, e, info);
            }
        } else {
            for (Element el : doc.getElements()) {
                el.onClick(this, e, info);
            }
        }
    }

    @Override
    public void mousePressed(final MouseEvent e) {
        info.mouseDown = true;
        startClick = e.getPoint();
        deselectAll();
        for (Element el : doc.getElements()) {
            el.onMousePressed(e, info);
        }
        repaint();
    }

    @Override
    public void mouseReleased(final MouseEvent e) {
        info.mouseDown = false;
        endClick = e.getPoint();
        final Set<Element> markedForRemove = new HashSet<>();
        for (Element el : doc.getElements()) {
            //Remove me if i'm the selected guide and i overlapping another guide
            if (el.isSelected() && el instanceof Guide) {
                for (Guide g : doc.getGuides()) {
                    if (el != g && ((Guide) el).overlap(g)) {
                        markedForRemove.add(el);
                    }
                }
            }
            //Releasing
            el.onMouseReleased(this, e, info);
            if (el.toRemove()) {
                markedForRemove.add(el);
            }
        }
        doc.removeAll(markedForRemove);
        repaint();
    }

    @Override
    public void mouseDragged(final MouseEvent e) {
        info.mouseX = e.getX();
        info.mouseY = e.getY();
        info.mouseDown = true;

        //Vertical guides
        if (doc.noneSelected() && (startClick.x < info.pageLeft || startClick.x > info.pageRight)) {
            doc.add(new VtGuide(startClick.x).created());
        } //Horizontal guides
        else if (doc.noneSelected() && (startClick.y < info.pageTop || startClick.y > info.pageBottom)) {
            doc.add(new HzGuide(startClick.y).created());
        }

        //elements
        for (Element el : doc.getElements()) {
            el.onMouseDragged(startClick, e, info);
            if (!(el instanceof Guide) && (el.isMoving() || el.isResizing())) {
                //snap to center
                if (el.getCenter() > info.pageCenter - HANDLE && el.getCenter() < info.pageCenter + HANDLE) {
                    el.snapCenterTo(startClick, info.pageCenter);
                }
                //Snap to guides
                for (Element g : doc.getElements()) {
                    if (g instanceof VtGuide) {
                        if (el.getLeft() > g.getRight() && el.getLeft() < g.getRight() + HANDLE) {
                            el.snapLeftTo(startClick, g.getRight());
                        }
                        if (el.getRight() > g.getLeft() - HANDLE && el.getRight() < g.getLeft()) {
                            el.snapRightTo(startClick, g.getLeft());
                        }
                    }
                    //todo hzGuide
                }
            }
        }
        repaint();
    }

    @Override
    public void mouseMoved(final MouseEvent e) {
        info.mouseX = e.getX();
        info.mouseY = e.getY();
        info.mouseDown = false;

        Cursor hover = new Cursor(Cursor.DEFAULT_CURSOR);
        for (Element el : doc.getElements()) {
            el.onMouseMoved(startClick, e, info);
            if (el.isHovered()) {
                hover = el.getHoveredCursor();
            }
        }
        this.setCursor(hover);
        repaint();
    }

    @Override
    public void mouseEntered(final MouseEvent e) {
        //do nothing
    }

    @Override
    public void mouseExited(final MouseEvent e) {
        //do nothing
    }
}
