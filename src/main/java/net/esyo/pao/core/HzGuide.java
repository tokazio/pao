package net.esyo.pao.core;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import static net.esyo.pao.core.HorizontalAlign.CENTER;
import static net.esyo.pao.core.VerticalAlign.BOTTOM;
import static net.esyo.pao.core.VerticalAlign.TOP;

/**
 *
 * @author romainpetit
 */
public class HzGuide extends AbstractGuide {

    private static final int HANDLE = 2;

    public HzGuide(final int y) {
        super(-1, y, -1, -1);
    }

    @Override
    public void draw(final Graphics2D g, final PageInfo info) {
        super.draw(g, info);
        g.drawLine(0, getY(), info.getCanvasWidth(), getY());
    }

    @Override
    public void drawPosition(final Graphics2D g, final PageInfo info) {
        Utils.drawText(g, info.getMouseX(), info.getMouseY() - 20, Integer.toString(getY() - info.getPageTop()), CENTER, BOTTOM);
        Utils.drawText(g, info.getMouseX(), info.getMouseY() + 20, Integer.toString(info.getPageBottom() - getY()), CENTER, TOP);
    }

    @Override
    protected boolean inPage(final PageInfo info) {
        return getY() > info.getPageTop() && getY() < info.getPageBottom();
    }

    @Override
    boolean isRemovable(final PageInfo info) {
        return getY() < info.getPageTop() || getY() > info.getPageBottom();
    }

    @Override
    public boolean inRect(final Point point) {
        return point.y > getY() - HANDLE && point.y < getY() + HANDLE;
    }

    @Override
    public boolean overlap(final Guide guide) {
        if (guide != this && guide instanceof HzGuide) {
            return guide.getTop() >= getY() - HANDLE && guide.getBottom() <= getY() + HANDLE;
        }
        return false;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public int getLeft() {
        throw new UnsupportedOperationException("Horizontal guide has no left side.");
    }

    @Override
    public int getRight() {
        throw new UnsupportedOperationException("Horizontal guide has no right side.");
    }

    @Override
    public int getTop() {
        return this.y;
    }

    @Override
    public int getBottom() {
        return this.y;
    }

    @Override
    public Cursor getHoveredCursor() {
        return new Cursor(Cursor.S_RESIZE_CURSOR);
    }

    @Override
    public void onMouseDragged(final Point start, final MouseEvent e, final PageInfo info) {
        super.onMouseDragged(start, e, info);
        if (isMoving()) {
            this.y = this.startY + e.getPoint().y - start.y;
        }
    }

}
